const express = require('express');
const mongoose = require('mongoose');
const app = express()

const cors = require('cors')
app.use(cors())
require('dotenv').config()

const port = process.env.port
const db = process.env.booking

mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
   
});

mongoose.connection.once('open', () => console.log("now connected to mongdb atlas"))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// we need to define the routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')
app.use('/api/users', userRoutes)
app.use('/api/courses',courseRoutes)

app.listen(port, () => {
    console.log("Api is Online");
})
