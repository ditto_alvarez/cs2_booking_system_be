const User = require('../models/user') //lets acquire the needed dependencies.
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Course = require('../models/course')

//;ets create a validation to make sure that no similar email address will be included / stored. 
module.exports.emailExists = (params) => {
   //what will happen in this function?
   //upon getting the input data from the user, our task is to run a search function/query inside the database,
   return User.find({ email: params.email }).then(result => {
      return result.length > 0 ? true : false 
   })
}

//create a function that will allow us to register a new user. 
module.exports.register = (params) => {
    let user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        mobileNo: bcrypt.hashSync(params.mobileNo, 8),
        password: bcrypt.hashSync(params.password, 10)   
        //Lowest value = 8 , highest = 31
        //cost?? 	-> the 2nd parameter = salt (describes the number of rounds)
        //take note: when you are hashing data, the moidule will go through a series of "rounds/iteration" just to hash the data
    })
   return user.save().then((user, err) => {
      if (err) console.log(err)
    	return (err) ? false : true
    }) //lets create a promise that will determine the return upon success or failure in saving the new entry.
}

//the next function is to allow a registered user to log in our app.
module.exports.login = (params) => {
  //the first thing that we are going to do is to check if the user exists base from our existing records.
  //1 -> email add
  //2 -> password
  return User.findOne({ email: params.email }).then(user => {
        //upon getting the email parameter of the user, the entered data has to pass the following requirements. 
        if(user === null) { return false }
        //if the email exists continue the process by checking if the passwords match each other. 
        const isPasswordMatched = bcrypt.compareSync(params.password, user.password) 
        if(isPasswordMatched) {
           return { access: auth.createAccessToken(user.toObject()) }
        } else { //why did we convert the document to an object()? because the document that we will recieve from the db is in json format. hindi ma acccess dahil cannot be parsed. 
           return false; 
        }
  })
      
}

//this function will allow us to diplay the users information upon logging in.
module.exports.get = (params) => {
   return User.findById(params.userId).then(user => {
      user.password = undefined//is to block the password value before it even reaches the browser. //keep the value of password secured.
       return user
   })
}

module.exports.get_all = (params) => {
   return User.find(function (err, res) {
      return res
   })
}

//lets create a function that will allow us to add a specific course to the user
module.exports.enroll = (params) => {
   return User.findById(params.userId).then(user => {
     user.enrollments.push({ courseId: params.courseId })
     //save the changes inside the database.
     return user.save().then((user, err) => {
       //this next section will allow us to insert the user insde the Course collection to have a feature that will allow you to identify the students enrolled in a course.
       return Course.findById(params.courseId).then(course => {
          course.enrollees.push({ userId: params.userId })

          return course.save().then((course, err) => {
              return (err) ? false : true 
          })
       })
     })
   })
}
