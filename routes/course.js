const express = require('express')
const router = express.Router()//lets acquire the dependencies needed. 
const CourseController = require('../controllers/course'); 

//lets create a ENDPOINT for our get all function which will allow us to display all of the courses available for enrollment. 
router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses)); 
})

//lets create a route that will allow us to send a request to create a new course.
router.post('/addCourse', (req, res) => {
   CourseController.insert(req.body).then(result => res.send(result)); 
})

//task is to create a route to get a single course from the db.
//the endpoint for this route will be a "placeholder parameter"
router.get('/:id', (req, res) => {
    const courseId = req.params.id
    CourseController.get({ courseId }).then(course => res.send(course))//lets capture the input of the user
})

router.post('/name-exists', (req, res) => {
    CourseController.courseExists(req.body).then(result => res.send(result));
})

router.delete('/delete/:courseId',CourseController.delete);

module.exports = router;