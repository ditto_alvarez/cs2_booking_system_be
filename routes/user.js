const express = require('express');
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')
//lets identify the needed dependencies to create our routes.


//[SECTION] Primary Routes
router.post('/email_exists', (req, res) => {
   //inside the body section lets describe what will be the procedure upon sending a request ib the route.
   UserController.emailExists(req.body).then(result => res.send(result));
})


//register a new user
router.post('/register', (req, res) => {
    UserController.register(req.body).then(result => res.send(result));
})

//lets create a route for our login method.
router.post('/login', (req, res) => {
  UserController.login(req.body).then(result => res.send(result))
})

//lets create a route that will send a request to display the user's information.
//upon getting the user, the user has to be authenticated to determine the proper authorization rights for the user
router.get('/details', auth.verify, (req, res) => {
   //get the headers section of the request with the JWT.
   const user = auth.decode(req.headers.authorization) //we are stripping away the header section of the data to decode the token to get the payload.
   UserController.get({ userId: user.id}).then(user => res.send(user))
}) 

router.get('/get_users', (req, res) => {
   UserController.get_all().then(user => res.send(user))
})

router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	UserController.enroll(params).then(result => res.send(result))
}) 




//[SECTION] Secondary Routes
module.exports = router; 
