const Course = require('../models/course')

//lets create a function that will allow us to add a new course inside the database. 

module.exports.insert = (params) => {
  let course = new Course({
  	name: params.name,
  	description: params.description,
  	price: params.price
  }) 
  //once you have successfully captured the data inserted by the user via the request body, its now time to save it. 
  return course.save().then((course, err) => {
     return (err) ? false : true 
  })
}

//we are going to create a new function that will display all of the courses that has an active status of "true". 
module.exports.getAll = () => {
    return Course.find({ isActive: true }).then(courses => courses)
}

//we are going to create anothe function that will display a single course. 
module.exports.get = (params) => {
    return Course.findById(params.courseId).then(course => course)
}

module.exports.courseExists = (params) => {
  return Course.find({ name: params.name }).then(result => {
      return result.length > 0 ? true : false
  })
}

module.exports.delete = (req,res) =>{
  Course.findByIdAndDelete(req.params.courseId)
      .then(data => {
          if(!data){
              res.status(404).send({ message : `Cannot Delete with id. Maybe id is wrong`})
          }else{
              res.send({
                  message : "Course was deleted successfully!"
              })
          }
      })
      .catch(err =>{
          res.status(500).send({
              message: "Could not delete User with id=" + id
          });
      });
}