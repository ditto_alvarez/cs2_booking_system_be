const mongoose = require('mongoose')

//we are going to create a schema/blueprint for our users.
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'firstName is required']
	},
	lastName: {
		type: String,
		required: [true, 'lastName is required']
	},
	email: {
		type: String,
		required: [true, 'email is required']
	},
	mobileNo: {
		type: String,
		required: [true, 'mobileNo is required']
	},
	password: {
		type: String,
		required: [true, 'password is required']
	}, 
	isAdmin: {
		type: Boolean,
		default: false 
	}, //this will be an array of objects, a student can enroll to more than 1 subject. 
	enrollments: [
	   {
	   	  courseId: {
	   	  	type: String,
	   	  	required: [true, "Course id is required"]
	   	  },  //this will identify the course/subject
	   	  enrolledOn: {
	   	  	type: Date,
	   	  	default: new Date()
	   	  }, //this will describe the time and day when the student enrolled for the subject.
	   	  status: {
	   	  	type: String,
	   	  	default: 'Enrolled' //alternative values (completed or canceled)
	   	  }     //this will describe if the student is accepted or rejected in the class.
	   }
	]
})

//now that we have defined the structure of the schema, lets now create a model. 
module.exports = mongoose.model('User', userSchema, 'user')
